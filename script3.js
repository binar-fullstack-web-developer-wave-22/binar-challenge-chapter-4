const choices = ['rock', 'paper', 'scissor'];

// get element
const cRock = document.querySelector('.com-rock');
const cPaper = document.querySelector('.com-paper');
const cScissor = document.querySelector('.com-scissor');
const divP1 = document.querySelector('.player-1');
const vsText = document.querySelector('.versus');
const resultText = document.getElementById('finalres');
const selectBtn = document.querySelector('.rock');
const selectBtn2 = document.querySelector('.paper');
const selectBtn3 = document.querySelector('.scissor');

function getComItem() {
  const com = Math.floor(Math.random() * 3);
  if (com == 0) {
    cRock.style.backgroundColor = '#c4c4c4';
  }
  if (com == 1) {
    cPaper.style.backgroundColor = '#c4c4c4';
  }
  if (com == 2) {
    cScissor.style.backgroundColor = '#c4c4c4';
  }
  return choices[com];
}

// DOM
function win() {
  vsText.innerHTML = 'PLAYER 1 WIN ';
  resultText.style.background = '#4C9654';
  resultText.style.transform = 'rotate(-28.87deg)';
  resultText.style.width = '270px';
  resultText.style.height = '160px';
  resultText.style.fontSize = '50px';
  resultText.style.color = 'white';
  resultText.style.textAlign = 'center';
  resultText.style.borderRadius = '10px';
  resultText.style.verticalAlign = 'middle';
}

function lose() {
  vsText.innerHTML = 'COM WIN ';
  resultText.style.background = '#4C9654';
  resultText.style.transform = 'rotate(-28.87deg)';
  resultText.style.verticalAlign = 'middle';
  resultText.style.letterSpacing = '6px';
  resultText.style.padding = '-10px';
  resultText.style.fontFamily = 'Open Sans';
  resultText.style.width = '270px';
  resultText.style.height = '160px';
  resultText.style.fontSize = '55px';
  resultText.style.color = 'white';
  resultText.style.textAlign = 'center';
  resultText.style.borderRadius = '10px';
}

function draw() {
  vsText.innerHTML = 'DRAW ';
  resultText.style.background = '#035B0C';
  resultText.style.transform = 'rotate(-28.87deg)';
  resultText.style.padding = '30px';
  resultText.style.width = '270px';
  resultText.style.height = '160px';
  resultText.style.fontSize = '60px';
  resultText.style.color = 'white';
  resultText.style.textAlign = 'center';
  resultText.style.borderRadius = '10px';
  resultText.style.verticalAlign = 'middle';
}

function getResultText() {
  vsText.style.fontSize = '25px';
  vsText.style.color = 'white';
  vsText.style.backgroundColor = '#4C9654';
  vsText.style.transform = 'rotate(-15deg)';
  vsText.style.width = '150px';
  vsText.style.margin = '0px 0px 0px 50px';
  vsText.style.borderRadius = '10px';
  vsText.style.padding = '10px';
}

function getResult(playerone, com) {
  if (playerone == com) return draw();
  if (playerone == 'rock') return com == 'paper' ? lose() : win();
  if (playerone == 'paper') return com == 'rock' ? win() : lose();
  if (playerone == 'scissor') return com == 'paper' ? win() : lose();
}

const pRock = document.querySelector('.rock');
pRock.addEventListener('click', function () {
  const oneChoice = pRock.className;
  const comChoice = getComItem();
  const result = getResult(oneChoice, comChoice);
  pRock.style.backgroundColor = '#c4c4c4';
});

const pPaper = document.querySelector('.paper');
pPaper.addEventListener('click', function () {
  const oneChoice = pPaper.className;
  const comChoice = getComItem();
  const result = getResult(oneChoice, comChoice);
  pPaper.style.backgroundColor = '#c4c4c4';
});

const pScissor = document.querySelector('.scissor');
pScissor.addEventListener('click', function () {
  const oneChoice = pScissor.className;
  const comChoice = getComItem();
  const result = getResult(oneChoice, comChoice);
  pScissor.style.backgroundColor = '#c4c4c4';
});
